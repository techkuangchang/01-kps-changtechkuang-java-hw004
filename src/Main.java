import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args){

        Wrap<Integer> integerWrap = new Wrap<>();

        try {
            integerWrap.addItem(1);
            integerWrap.addItem(1);
        }catch (Exception e){
            System.out.println(e);
        }

        System.out.println("\nList all from inputted");
        for (int i=0; i<integerWrap.list.size();i++) {
            System.out.println(integerWrap.getItem(i));
        }
    }

    //------------------------------Inner Class ---------------------------------
    //Wrap Class
    static class Wrap<T> {

        public List<T> list = new ArrayList<>();

        public void addItem(T l) {
            try {
                if (l == null) {
                    throw new NumberFormatException("Inputted null value");
                }
                if(list.contains(l)) { //contains use for check equals string
                    throw new DupicateException("Duplicate Value : " + l);
                }
                    list.add(l); //add value
            }catch (DupicateException ex){
                System.out.print("DuplicateException : ");
                System.out.print(ex.getMessage());  //print message DuplicateException object
            }
        }

        public T getItem(int n) {
            return list.get(n);
        }
    }

    //Create Custom Duplicate Exception
    static class DupicateException extends Exception{
        public DupicateException(String s){
            super(s);
        }
    }
}



